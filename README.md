# mirroring-win

Mirroring folders on a windows 10 os for backup using robocopy.

Settings
--------
Before first usage, the destination directory (DESTINATION_DIR) 
and source directory (SOURCE_DIR) has to be set in the file backup.bat:

	@SET SOURCE_DIR=Drive:\path_to_source_directory\
	@SET DESTINATION_DIR=Drive:\path_to_destination_directory\
	
Here, absolute file paths are given.


Usage
-----

To list all files included in the mirroring process:

	backup list

Details for each file copied or skipped are logged in backup-protocol.log.
This is just to show changes to be performed.
NO BACKUP IS DONE!

To execute mirroring:

	backup make

New files from source directory are mirrored to destination directory.
Changes in the backup directory are overwritten with 
the content of pre-existing files in the source directory.
File names only existing in the destination directory are skipped.
They will remain in the destination directory without any changes 
and listed as EXTRA files and errors in the log file.
Details for each file copied or skipped are again logged in backup-protocol.log.

All other arguments are ignored with an error message.


