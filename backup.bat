@echo off

::to be changed:
@SET SOURCE_DIR=D:\path_to_source_directory\
@SET DESTINATION_DIR=K:\path_to_destination_directory\

@SET LOG_FILE=backup-protocol.log

REM mirroring when error: 5 repeats and 5sec wait inbetween 
REM copy all file properties (DAT which is default without SOU) and directory properties (DAT)
REM do not remove files existing only in destination directory (/XX)
REM verbose output source file time stamps and full path names are included
@SET CP_FLAGS=/MIR /XX /R:5 /W:5 /COPY:DAT /DCOPY:DAT /V /TS /FP


echo Batch file to mirror files and complete folder structure

:: select script with arguments
if [%1] == [] echo NOTHING WILL BE DONE!! & echo NO ARGUMENTS GIVEN: chose list or make & goto :NO_ARGUMENT
if /I %1 == list goto :LIST
if /I %1 == make goto :MAKE

echo no valid argument given: chose list or make
goto :NO_ARGUMENT

:LIST
echo LIST
echo NO CHANGES WILL BE MADE!! Just listing indexed files for backup mirroring.
echo source directory: %SOURCE_DIR%  & echo destination directory: %DESTINATION_DIR%

robocopy %SOURCE_DIR% %DESTINATION_DIR% %CP_FLAGS% /l /LOG:%LOG_FILE%

goto :NO_ARGUMENT


:MAKE
echo MAKE
echo CAREFULLY chose source and destination: Destination files will be overwritten
echo check with list as argument before
echo source directory: %SOURCE_DIR%  & echo destination directory: %DESTINATION_DIR%
pause

REM put output to log file
robocopy %SOURCE_DIR% %DESTINATION_DIR% %CP_FLAGS% /LOG:%LOG_FILE%

goto :NO_ARGUMENT


:NO_ARGUMENT
echo Finished.
pause


